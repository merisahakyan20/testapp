﻿using Core.Database;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public static class ApiCallManager
    {
        /// <summary>
        /// Gets data from given url and parses json into Kline database class
        /// Returns null if something went wrong
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static async Task<List<Kline>> GetAsync(string url)
        {
            try
            {
                List<Kline> result = new List<Kline>();
                List<List<string>> fullData;

                using (HttpClient client = new HttpClient())
                {
                    var data = await client.GetAsync(url);
                    var resultJson = await data.Content.ReadAsStringAsync();

                    fullData = JsonConvert.DeserializeObject<List<List<string>>>(resultJson).ToList();
                    foreach (var obj in fullData)
                    {
                        if (obj.Count >= 11)
                        {
                            result.Add(new Kline
                            {
                                OpenTime = obj[0],
                                Open = obj[1],
                                High = obj[2],
                                Low = obj[3],
                                Close = obj[4],
                                Volume = obj[5],
                                CloseTime = obj[6],
                                QuoteVolume = obj[7],
                                TradesCount = obj[8],
                                TakerBaseVolume = obj[9],
                                TakerQuoteVolume = obj[10],
                            });
                        }
                    }
                }
                return result;
            }
            catch
            {
                return null;
            }
        }
    }
}
