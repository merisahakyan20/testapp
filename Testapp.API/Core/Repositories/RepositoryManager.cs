﻿using Core.Database;
using Core.RepositoryInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Repositories
{
    public class RepositoryManager : IRepositoryManager
    {
        private readonly ApplicationContext _context;
        public RepositoryManager(ApplicationContext context)
        {
            _context = context;
        }

        private IKlinesRepository _klines;
        public IKlinesRepository Klines => _klines = new KlinesRepository(_context);
    }
}
