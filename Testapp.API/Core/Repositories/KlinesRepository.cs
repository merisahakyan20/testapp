﻿using Core.Database;
using Core.RepositoryInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Repositories
{
    public class KlinesRepository : RepositoryBase<Kline>, IKlinesRepository
    {
        public KlinesRepository(ApplicationContext context) : base(context)
        {

        }
    }
}
