﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database
{
    public class ApplicationContext : DbContext
    {
        private readonly string schema;

        public ApplicationContext(string schema)
          : base("BinanceDb")
        {
            this.schema = schema;
        }

        public DbSet<Kline> Klines { get; set; }

        protected override void OnModelCreating(DbModelBuilder builder)
        {
            builder.HasDefaultSchema(this.schema);
            base.OnModelCreating(builder);
        }
    }

}
