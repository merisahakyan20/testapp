﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database
{
    public class Kline : BaseEntity
    {
        public string OpenTime { get; set; }
        public string Open { get; set; }
        public string High { get; set; }
        public string Low { get; set; }
        public string Close { get; set; }
        public string Volume { get; set; }
        public string CloseTime { get; set; }
        public string QuoteVolume { get; set; }
        public string TradesCount { get; set; }
        public string TakerBaseVolume { get; set; }
        public string TakerQuoteVolume { get; set; }
    }
}
