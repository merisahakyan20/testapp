﻿//Create Database 
create database BinanceDb

//Create Klines table
create table public."Klines"(
"Id" serial primary key ,
"OpenTime" varchar(256) not null,
"Open" varchar(256) not null,
"High" varchar(256) not null, 
"Low" varchar(256) not null, 
"Close" varchar(256) not null, 
"Volume" varchar(256) not null, 
"CloseTime" varchar(256) not null, 	
"QuoteVolume" varchar(256) not null, 
"TradesCount" varchar(256) not null, 
"TakerBaseVolume" varchar(256) not null, 
"TakerQuoteVolume" varchar(256) not null
);