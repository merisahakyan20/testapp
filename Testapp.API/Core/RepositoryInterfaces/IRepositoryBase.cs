﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.RepositoryInterfaces
{
    public interface IRepositoryBase<T>
    {
        IQueryable<T> GetAll();
        T GetSingle(int id);
        void Add(T entity);
        void Delete(int id);
        Task SaveChangesAsync();
    }
}
