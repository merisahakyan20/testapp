﻿using Core;
using Core.Database;
using Core.RepositoryInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Test.API.Controllers
{
    public class HomeController : ApiController
    {
        readonly IRepositoryManager _repoManager;
        public HomeController(IRepositoryManager repositoryManager)
        {
            _repoManager = repositoryManager;
        }

        /// <summary>
        /// Calls to api, gets data and saves into database
        /// </summary>
        /// <returns>Returns Ok if operation successed and internal server error if something went wrong</returns>
        [HttpGet]
        public async Task<HttpResponseMessage> GetAsync()
        {
            try
            {
                var requestUrl = "https://api.binance.com/api/v1/klines?symbol=ETHBTC&interval=1m&limit=10";
                var result = await ApiCallManager.GetAsync(requestUrl);

                result.ForEach(o => _repoManager.Klines.Add(o));

                await _repoManager.Klines.SaveChangesAsync();

                return new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.OK,
                };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.InternalServerError,
                    Content = new StringContent(ex.Message)
                };
            }
        }
    }
}
