﻿using Quartz;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace Test.API.Scheduller
{
    public class ApiCaller : IJob
    {
        /// <summary>
        /// Calls to Get endpoint
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public async Task Execute(IJobExecutionContext context)
        {
            using (HttpClient client = new HttpClient())
            {
                var url = "http://localhost:2158/api/home";
                await client.GetAsync(url);
            }
        }
    }
}