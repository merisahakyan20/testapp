﻿using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Test.API.Scheduller
{
    public class JobScheduler
    {
        static IScheduler scheduler = StdSchedulerFactory.GetDefaultScheduler().Result;

        /// <summary>
        /// Starts scheduller which will work every minute from now
        /// </summary>
        public static void Start()
        {
            try
            {
                var schedFact = new StdSchedulerFactory();

                // get a scheduler, start the schedular before triggers or anything else
                var sched = schedFact.GetScheduler().Result;
                sched.Start();

                // create job
                var job = JobBuilder.Create<ApiCaller>()
                        .WithIdentity("caller", "data")
                        .Build();

                //trigger working every minute 
                var dateNow = DateTime.Now;
                var trigger = TriggerBuilder.Create()
                                             .WithDailyTimeIntervalSchedule
                                                     (s =>
                                                        s.WithIntervalInMinutes(1)
                                                       .OnEveryDay()
                                                       .StartingDailyAt(TimeOfDay.HourAndMinuteOfDay(dateNow.Hour, dateNow.Minute))
                                                     )
                                              .Build();

                sched.ScheduleJob(job, trigger);


            }
            catch (Exception ex)
            {
            }
        }

    }
}