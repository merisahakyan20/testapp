﻿using Core.Database;
using Core.Repositories;
using Core.RepositoryInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Web;
using System.Web.Http;
using System.Web.Routing;
using Test.API.Scheduller;
using Unity;
using Unity.Lifetime;
using Unity.WebApi;

namespace Test.API
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);

            //Dependency injection configuration
            var container = new UnityContainer();
            container.RegisterType<IRepositoryManager, RepositoryManager>();
            container.RegisterType<IKlinesRepository, KlinesRepository>();
            container.RegisterInstance<ApplicationContext>(new ApplicationContext("public"));
            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);

            JobScheduler.Start();
        }
    }
}
