﻿using System;
using Core.Database;
using Core.Repositories;
using Core.RepositoryInterfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Test.API.Controllers;

namespace CoreTests
{
    [TestClass]
    public class HomeControllerTest
    {
        IRepositoryManager repoManager;
        ApplicationContext context;
        HomeController controller;

        [TestInitialize]
        public void TestSetup()
        {
            context = new ApplicationContext("HomeControllerTest");
            repoManager = new RepositoryManager(context);
            controller = new HomeController(repoManager);
            MockContext.MockDatabase(context);
        }
        [TestMethod]
        public void GetTest()
        {
            //Act
            var result = controller.GetAsync().Result;

            //Assert
            Assert.AreEqual(result.StatusCode, System.Net.HttpStatusCode.OK);

        }

        [TestMethod]
        public void GetTest_Error()
        {
            //Arrange
            controller = new HomeController(null);

            //Act
            var result = controller.GetAsync().Result;

            //Assert
            Assert.AreEqual(result.StatusCode, System.Net.HttpStatusCode.InternalServerError);

        }
    }
}
