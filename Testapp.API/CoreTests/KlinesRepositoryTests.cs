﻿using System;
using Core.Database;
using Core.Repositories;
using Core.RepositoryInterfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace CoreTests
{
    [TestClass]
    public class KlinesRepositoryTests
    {
        IKlinesRepository repository;
        ApplicationContext context;

        [TestInitialize]
        public void TestSetup()
        {
            context = new ApplicationContext("KlinesRepositoryTest");
            repository = new KlinesRepository(context);
            MockContext.MockDatabase(context);
        }

        [TestMethod]
        public void AddTest()
        {
            //Arrange
            var kline = MockContext.NewKline();

            //Act 
            repository.Add(kline);
            var result = repository.SaveChangesAsync();
            result.Wait();
            var completed = result.IsCompleted;

            //Assert
            Assert.IsTrue(kline.Id != 0);
            Assert.IsTrue(completed);
        }        

        [TestMethod]
        public void GetAllTest()
        {
            //Arrange

            //Act 
            var result = repository.GetAll();

            //Assert
            Assert.IsNotNull(result);
            Assert.IsTrue(result.Count() > 0);
        }

        [TestMethod]
        public void GetSingleTest()
        {
            //Arrange
            var id = MockContext.Klines().Last().Id;

            //Act 
            var result = repository.GetSingle(id);

            //Assert
            Assert.IsNotNull(result);
            Assert.IsTrue(result.Id == id);
        }

        [TestMethod]
        public void DeleteTest()
        {
            //Arrange
            var id = MockContext.Klines().First().Id;

            //Act 
            repository.Delete(id);
            var result = repository.SaveChangesAsync();
            result.Wait();
            var completed = result.IsCompleted;

            //Assert
            Assert.IsNull(context.Klines.FirstOrDefault(k => k.Id == id));
            Assert.IsTrue(completed);
        }
    }
}
