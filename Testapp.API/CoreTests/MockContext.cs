﻿using Core.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreTests
{
    public class MockContext
    {
        public static List<Kline> Klines()
        {
            return new List<Kline>()
            {
                new Kline
                {
                    Id=1,
                    Close = "",
                    CloseTime = "",
                    High = "",
                    Low = "",
                    Open = "",
                    OpenTime = "",
                    QuoteVolume = "",
                    TakerBaseVolume = "",
                    TakerQuoteVolume = "",
                    TradesCount = "",
                    Volume = ""
                },
                new Kline
                {
                    Id=2,
                    Close = "",
                    CloseTime = "",
                    High = "",
                    Low = "",
                    Open = "",
                    OpenTime = "",
                    QuoteVolume = "",
                    TakerBaseVolume = "",
                    TakerQuoteVolume = "",
                    TradesCount = "",
                    Volume = ""
                },
            };
        }

        public static Kline NewKline()
        {
            return new Kline
            {
                Close = "",
                CloseTime = "",
                High = "",
                Low = "",
                Open = "",
                OpenTime = "",
                QuoteVolume = "",
                TakerBaseVolume = "",
                TakerQuoteVolume = "",
                TradesCount = "",
                Volume = ""
            };
        }
        public static void MockDatabase(ApplicationContext context)
        {
            context.Klines.AddRange(Klines());
            context.SaveChanges();
        }
    }
}
