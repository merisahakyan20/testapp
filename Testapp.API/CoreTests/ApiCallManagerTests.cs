﻿using System;
using Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CoreTests
{
    [TestClass]
    public class ApiCallManagerTests
    {
        [TestMethod]
        public void GetAsync_Ok()
        {
            //Arrange
            var url = "https://api.binance.com/api/v1/klines?symbol=ETHBTC&interval=1m&limit=10";

            //Act
            var result = ApiCallManager.GetAsync(url).Result;

            //Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void GetAsync_Fail()
        {
            //Arrange
            var url = "https://api.binance.com/api/v1/klines?symbol=ETHBTC&interval=1m&limit=";

            //Act
            var result = ApiCallManager.GetAsync(url).Result;

            //Assert
            Assert.IsNull(result);
        }
    }
}
